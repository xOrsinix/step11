package com.example.application3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider

lateinit var viewModel: TestViewModel
lateinit var tvNumber:TextView
lateinit var btUpdate:Button

class SecretActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_secret)
        tvNumber=findViewById(R.id.tvNumber)
        btUpdate=findViewById(R.id.btUpdate)

        viewModel = ViewModelProvider(this).get(TestViewModel::class.java)

        viewModel.currentNumber.observe(this, Observer {
            tvNumber.text=it.toString()
        })

        incrementText()
    }

    private fun incrementText(){
        btUpdate.setOnClickListener(object: View.OnClickListener{
            override fun onClick(p0: View?) {
                viewModel.currentNumber.value=++viewModel.number
            }

        })
    }
}