package com.example.application3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

private lateinit var tvText: TextView

class InfoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)
        var lines=listOf("","","18","","")
        var dataPerson=intent.getStringExtra(Constants.id)
        val age=intent.getIntExtra(Constants.idFI,20)
        if (dataPerson==null){
            lines=listOf("","","","")
        }else{
            lines=dataPerson.lines()
        }

        tvText=findViewById(R.id.tvText)
        when (age){
            in 0..18 -> tvText.text="Вам от 0 до 18 лет\nВаше полное ФИО: ${lines[0]} ${lines[1]} ${lines[2]}\nВам ${age} лет\n" +
                    "Ваши увлечения: ${lines[3]}"
            in 19..35 -> tvText.text="Вам от 19 до 35 лет\nВаше полное ФИО: ${lines[0]} ${lines[1]} ${lines[2]}\nВам ${age} лет\n" +
                    "Ваши увлечения: ${lines[3]}"
            in 36..65 -> tvText.text="Вам от 36 до 65 лет\nВаше полное ФИО: ${lines[0]} ${lines[1]} ${lines[2]}\nВам ${age} лет\n" +
                    "Ваши увлечения: ${lines[3]}"
            in 66..100 -> tvText.text="Вам от 66 до 100 лет\nВаше полное ФИО: ${lines[0]} ${lines[1]} ${lines[2]}\nВам ${age} лет\n" +
                    "Ваши увлечения: ${lines[3]}"
            else -> tvText.text="Некорректный возраст\n"
        }
        tvText.text= tvText.text.toString()+"\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"+"\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" + "Privet" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n"
    }

}