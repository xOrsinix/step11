package com.example.application3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.ImageView
import kotlinx.coroutines.*

private lateinit var ivLoad:ImageView

class SplashActivity : AppCompatActivity() {

    suspend fun myDelay(){
        delay(3000L)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        ivLoad = findViewById(R.id.ivLoad)
        ivLoad.setImageResource(R.drawable.image)
        Handler().postDelayed({
            Intent(this,MainActivity::class.java).also{
                startActivity(it)
            }
        },3000)

    }
}