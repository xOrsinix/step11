package com.example.application3

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import kotlin.random.Random

private lateinit var etName:EditText
private lateinit var etSurname:EditText
private lateinit var etFName:EditText
private lateinit var etAge:EditText
private lateinit var etHobby:EditText
private lateinit var btNext:Button
private lateinit var cBcheck:CheckBox
private lateinit var tvName:TextView
private lateinit var CLay:ConstraintLayout

private var sList=""
private const val TAG="MainActivity"

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "TestApp"
        etName=findViewById(R.id.etName)
        etSurname=findViewById(R.id.etSurname)
        etFName=findViewById(R.id.etFName)
        etAge=findViewById(R.id.etAge)
        etHobby=findViewById(R.id.etHobby)
        btNext=findViewById(R.id.btNext)
        cBcheck=findViewById(R.id.cBcheck)
        tvName=findViewById(R.id.tvName)
        CLay=findViewById(R.id.CLay)

        val intent=Intent(this,InfoActivity::class.java)

        loadData()

        etName.addTextChangedListener(object:TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                Log.i(TAG,"nameChanged $p0")
            }

        })

        etSurname.addTextChangedListener(object:TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                Log.i(TAG,"surnameChanged $p0")
            }

        })

        etFName.addTextChangedListener(object:TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                Log.i(TAG,"fNameChanged $p0")
            }

        })

        etAge.addTextChangedListener(object:TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                Log.i(TAG,"ageChanged $p0")
            }

        })

        etHobby.addTextChangedListener(object:TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                Log.i(TAG,"hobbyChanged $p0")
            }

        })

        btNext.setOnClickListener(object: View.OnClickListener{
            override fun onClick(p0: View?) {
                if (cBcheck.isChecked) {
                    sList+= etName.text.toString()+'\n'+etSurname.text.toString()+'\n'+etFName.text.toString()+'\n'+ etHobby.text.toString()
                    intent.putExtra(Constants.id, sList)
                    intent.putExtra(Constants.idFI, etAge.text.toString().toInt())
                    startActivity(intent)
                }
            }

        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu,menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId){
            android.R.id.home -> finish()

            R.id.saveLines_menu-> {
                saveData()
            }

            R.id.setBackground_menu->{
                changePic(CLay)
            }

            R.id.goSecretActivity_menu->{
                val intentS=Intent(this,SecretActivity::class.java)
                startActivity(intentS)
            }
        }
        return true
    }

    private fun saveData(){
        val nameText= etName.text.toString()
        val surnameText= etSurname.text.toString()
        val fNameText= etFName.text.toString()
        val ageText= etAge.text.toString()
        val hobbyText= etHobby.text.toString()

        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.apply{
            putString("name_key",nameText)
            putString("surname_key",surnameText)
            putString("fname_key",fNameText)
            putString("age_key",ageText)
            putString("hobby_key",hobbyText)
        }.apply()

        Toast.makeText(this,"Data saved",Toast.LENGTH_LONG).show()
    }

    private fun loadData(){
        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        etName.setText(sharedPreferences.getString("name_key","Иван"))
        etSurname.setText(sharedPreferences.getString("surname_key","Иванов"))
        etFName.setText(sharedPreferences.getString("fname_key","Иванович"))
        etAge.setText(sharedPreferences.getString("age_key","18"))
        etHobby.setText(sharedPreferences.getString("hobby_key","Спорт"))
    }

    private fun changePic(CLay:ConstraintLayout){
        when (Random.nextInt(1,6)){
            1 -> CLay.setBackgroundResource(R.drawable.kotek1)
            2 -> CLay.setBackgroundResource(R.drawable.kotek2)
            3 -> CLay.setBackgroundResource(R.drawable.kotek3)
            4 -> CLay.setBackgroundResource(R.drawable.kotek4)
            5 -> CLay.setBackgroundResource(R.drawable.patric)
        }
    }
}